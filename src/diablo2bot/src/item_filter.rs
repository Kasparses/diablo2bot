use std::collections::HashMap;

use serde::{Deserialize, Serialize};

use crate::{enums::quality::Quality, token_template::TokenTemplate};

#[derive(Serialize, Deserialize, Debug)]
pub struct FilterItem {
    pub base_type: String,
    pub quality: Quality,
    pub texts: Vec<String>,
}

enum OperationNodeType {
    And,
    Or,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
enum Operator {
    GreaterThanOrEqualNumber(u32),
    EqualsText(String),
}

impl Operator {
    fn is_match(&self, actual_value: &str) -> bool {
        match self {
            Operator::GreaterThanOrEqualNumber(target_value) => actual_value
                .parse::<u32>()
                .map_or(false, |value| value >= *target_value),
            Operator::EqualsText(target_value) => actual_value == target_value,
        }
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
struct Condition {
    #[serde(rename = "Token")]
    token: String,
    #[serde(rename = "Operator")]
    operator: Operator,
}

impl Condition {
    fn is_match(&self, values: &HashMap<String, String>) -> bool {
        values
            .get(&self.token)
            .map_or(false, |value| self.operator.is_match(value))
    }
}

pub struct MatchTemplate {
    template: TokenTemplate,
    conditions: Vec<Condition>,
}

impl MatchTemplate {
    fn is_match(&self, text: &str) -> bool {
        match self.template.extract_values(text) {
            Some(values) => self.conditions.iter().all(|c| c.is_match(&values)),
            None => false,
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct MatchTemplateUnparsed {
    #[serde(rename = "Template")]
    template: String,
    #[serde(rename = "Conditions")]
    conditions: Vec<Condition>,
}

impl MatchTemplateUnparsed {
    fn to_match_template(&self) -> MatchTemplate {
        MatchTemplate {
            template: TokenTemplate::new(&self.template).unwrap_or_else(|| panic!("Should be able to parse template string \"{}\" into a TokenTemplate object. Please adjust the template.", self.template)),
            conditions: self.conditions.iter().cloned().collect(),
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub enum ItemFilterIntermediate {
    And(Vec<Self>),
    Or(Vec<Self>),
    InAnyFilter(Vec<String>),
    QualityIn(Vec<Quality>),
    BaseTypeIn(Vec<String>),
    TextContainsAll(Vec<String>),
    TextContainsAny(Vec<String>),
    MatchesTemplate(MatchTemplateUnparsed),
}

pub struct UnparsedItemFilterFiles {
    pub item_filters: ItemFilterIntermediate,
    pub shared_item_filters: HashMap<String, ItemFilterIntermediate>,
}

impl UnparsedItemFilterFiles {
    pub fn to_item_filter(&self) -> ItemFilter {
        self.item_filters.to_item_filter(&self.shared_item_filters)
    }
}

impl ItemFilterIntermediate {
    pub fn to_item_filter(
        &self,
        shared_item_filters: &HashMap<String, ItemFilterIntermediate>,
    ) -> ItemFilter {
        match self {
            Self::And(item_filter_intermediates) => ItemFilter::And(
                item_filter_intermediates
                    .iter()
                    .map(|i| i.to_item_filter(shared_item_filters))
                    .collect(),
            ),
            Self::Or(item_filter_intermediates) => ItemFilter::Or(
                item_filter_intermediates
                    .iter()
                    .map(|i| i.to_item_filter(shared_item_filters))
                    .collect(),
            ),
            Self::InAnyFilter(items) => ItemFilter::Or(
                items
                    .iter()
                    .map(|i| shared_item_filters.get(i).unwrap_or_else(|| panic!("Shared filter \"{}\" should be present in the shared_item_filters.json file. Please add the filter to the file or remove the reference from your loot filter.", i)).to_item_filter(shared_item_filters))
                    .collect(),
            ),
            Self::QualityIn(values) => ItemFilter::QualityIn(values.clone()),
            Self::BaseTypeIn(values) => ItemFilter::BaseTypeIn(values.clone()),
            Self::TextContainsAll(items) => ItemFilter::TextContainsAll(items.clone()),
            Self::TextContainsAny(items) => ItemFilter::TextContainsAny(items.clone()),
            Self::MatchesTemplate(match_template_unparsed) => ItemFilter::MatchesTemplate(match_template_unparsed.to_match_template())
        }
    }
}

pub enum ItemFilter {
    And(Vec<Self>),
    Or(Vec<Self>),
    QualityIn(Vec<Quality>),
    BaseTypeIn(Vec<String>),
    TextContainsAll(Vec<String>),
    TextContainsAny(Vec<String>),
    MatchesTemplate(MatchTemplate),
}

impl ItemFilter {
    pub fn is_match(&self, item: &FilterItem) -> bool {
        match self {
            Self::And(nodes) => nodes.iter().all(|n| n.is_match(item)),
            Self::Or(nodes) => nodes.iter().any(|n| n.is_match(item)),
            Self::QualityIn(values) => values.contains(&item.quality),
            Self::BaseTypeIn(values) => values.iter().any(|v| &item.base_type == v),
            Self::TextContainsAll(values) => values
                .iter()
                .all(|v| item.texts.iter().any(|t| t.contains(v))),
            Self::TextContainsAny(values) => values
                .iter()
                .any(|v| item.texts.iter().any(|t| t.contains(v))),
            Self::MatchesTemplate(match_templates) => {
                item.texts.iter().any(|t| match_templates.is_match(t))
            }
        }
    }

    pub fn get_filter_with_only_base_type_and_quality(&self) -> Option<Self> {
        match self {
            Self::And(nodes) => Self::filter_child_nodes(nodes, OperationNodeType::And),
            Self::Or(nodes) => Self::filter_child_nodes(nodes, OperationNodeType::Or),
            Self::QualityIn(values) => Some(Self::QualityIn(values.clone())),
            Self::BaseTypeIn(values) => Some(Self::BaseTypeIn(values.clone())),
            Self::MatchesTemplate(_) => None,
            Self::TextContainsAll(_) => None,
            Self::TextContainsAny(_) => None,
        }
    }

    fn filter_child_nodes(nodes: &[Self], node_type: OperationNodeType) -> Option<Self> {
        let mut nodes: Vec<Self> = nodes
            .iter()
            .filter_map(|c| c.get_filter_with_only_base_type_and_quality())
            .collect();

        if nodes.is_empty() {
            None
        } else if nodes.len() == 1 {
            nodes.pop()
        } else {
            match node_type {
                OperationNodeType::And => Some(Self::And(nodes)),
                OperationNodeType::Or => Some(Self::Or(nodes)),
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use std::collections::HashMap;

    use serde::{Deserialize, Serialize};

    use crate::{
        file_io::{read_and_deserialize_json_file, FileIo},
        item_filter::ItemFilter,
    };

    use super::{FilterItem, ItemFilterIntermediate, UnparsedItemFilterFiles};

    // cargo test test_item_filter --manifest-path src/diablo2bot/Cargo.toml -- --nocapture

    #[derive(Serialize, Deserialize, Debug)]
    struct TestItem {
        item: FilterItem,
        expected_in_base_filter: bool,
        expected_in_full_filter: bool,
    }

    #[test]
    fn test_item_filter_full() {
        let (item_filter, test_items) = read_data();

        for test_item in test_items.iter() {
            assert_eq!(
                test_item.expected_in_full_filter,
                item_filter.is_match(&test_item.item)
            );
        }
    }

    #[test]
    fn test_item_filter_base() {
        let (item_filter, test_items) = read_data();

        let item_filter = item_filter
            .get_filter_with_only_base_type_and_quality()
            .unwrap();

        for test_item in test_items.iter() {
            assert_eq!(
                test_item.expected_in_base_filter,
                item_filter.is_match(&test_item.item)
            );
        }
    }

    fn read_data() -> (ItemFilter, Vec<TestItem>) {
        let file_io = FileIo::new();
        let folder_path = file_io.root.join("test_data").join("item_filter");
        let item_filter_file_path = folder_path.join("item_filter.json");
        let shared_item_filters_file_path = folder_path.join("shared_item_filters.json");
        let test_items_file_path = folder_path.join("test_items.json");

        let item_filters: ItemFilterIntermediate =
            read_and_deserialize_json_file(&item_filter_file_path).unwrap();

        let shared_item_filters: HashMap<String, ItemFilterIntermediate> =
            read_and_deserialize_json_file(&shared_item_filters_file_path).unwrap();

        let unparsed_item_filter_files = UnparsedItemFilterFiles {
            item_filters,
            shared_item_filters,
        };

        let test_items: Vec<TestItem> =
            read_and_deserialize_json_file(&test_items_file_path).unwrap();

        (unparsed_item_filter_files.to_item_filter(), test_items)
    }
}
