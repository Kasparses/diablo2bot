use crate::{
    constants::game_window_areas::NOISY_AREAS,
    enums::{errors::WrongGameStateError, state::State},
    game::Game,
    game_screenshotter::GameScreenshotter,
    image::Image,
    matrix::Matrix,
    point_u16::PointU16,
    structs::Pixel,
    utils::sleep_frame,
};

pub struct ValidationPixel {
    pub point: PointU16,
    pub pixel: Pixel,
}

pub fn wait_for_enum_states(
    g: &mut Game,
    states: &[State],
    max_frame_wait_count: u32,
) -> Result<u32, WrongGameStateError> {
    for _ in 0..max_frame_wait_count {
        let img = g.game_screenshotter.take_screenshot();

        for (i, state) in states.iter().enumerate() {
            if state.is_active(&img) {
                return Ok(i as u32);
            }
        }

        sleep_frame();
    }

    Err(WrongGameStateError::new(
        &g.game_screenshotter.take_screenshot(),
    ))
}

pub fn wait_for_merchant_dialog_menu(
    g: &mut Game,
    max_frame_wait_count: u32,
) -> Result<Matrix, WrongGameStateError> {
    for _ in 0..max_frame_wait_count {
        let mut matrix = g
            .game_screenshotter
            .take_screenshot()
            .to_matrix(&g.palette_transformer);

        matrix.clear_areas(&NOISY_AREAS);

        let items = g.font_symbol_matcher.match_image_items(&matrix);
        for item in items {
            if item.name == "talk" {
                return Ok(matrix);
            }
        }

        sleep_frame();
    }

    Err(WrongGameStateError::new(
        &g.game_screenshotter.take_screenshot(),
    ))
}

pub fn wait_while_merchant_dialog_menu_open(
    g: &mut Game,
    max_frame_wait_count: u32,
) -> Result<Matrix, WrongGameStateError> {
    for _ in 0..max_frame_wait_count {
        let mut matrix = g
            .game_screenshotter
            .take_screenshot()
            .to_matrix(&g.palette_transformer);

        matrix.clear_areas(&NOISY_AREAS);

        let items = g.font_symbol_matcher.match_image_items(&matrix);

        let mut found_dialog_option = false;

        for item in items {
            if item.name == "talk" {
                found_dialog_option = true;
            }
        }

        if !found_dialog_option {
            return Ok(matrix);
        }

        sleep_frame();
    }

    Err(WrongGameStateError::new(
        &g.game_screenshotter.take_screenshot(),
    ))
}

pub fn wait_for_enum_state(
    g: &mut Game,
    state: State,
    max_frame_wait_count: u32,
) -> Result<Image, WrongGameStateError> {
    wait_for_enum_state_(&mut g.game_screenshotter, state, max_frame_wait_count)
}

pub fn wait_for_enum_state_(
    game_screenshotter: &mut GameScreenshotter,
    state: State,
    max_frame_wait_count: u32,
) -> Result<Image, WrongGameStateError> {
    for _ in 0..max_frame_wait_count {
        let img = game_screenshotter.take_screenshot();

        if state.is_active(&img) {
            return Ok(img);
        }

        sleep_frame();
    }

    Err(WrongGameStateError::new(
        &game_screenshotter.take_screenshot(),
    ))
}

pub fn wait_while_in_enum_state(
    g: &mut Game,
    state: State,
    max_frame_wait_count: u32,
) -> Result<Image, WrongGameStateError> {
    for _ in 0..max_frame_wait_count {
        let img = g.game_screenshotter.take_screenshot();

        if !state.is_active(&img) {
            return Ok(img);
        }

        sleep_frame();
    }

    Err(WrongGameStateError::new(
        &g.game_screenshotter.take_screenshot(),
    ))
}
