use std::collections::HashMap;

const OPENER: &str = "{{";
const CLOSER: &str = "}}";

#[derive(Debug)]
enum Token {
    Placeholder(String),
    Literal(String),
}

#[derive(Debug)]
pub struct TokenTemplate {
    tokens: Vec<Token>,
}

impl TokenTemplate {
    pub fn new(template_text: &str) -> Option<Self> {
        if template_text.is_empty() {
            return None;
        }

        let mut tokens = Vec::new();
        let mut position = 0;

        loop {
            let snippet = &template_text[position..];

            let start_offset = match snippet.find(OPENER) {
                Some(off) => off,
                None => {
                    if !snippet.is_empty() {
                        tokens.push(Token::Literal(snippet.to_owned()));
                    }
                    break;
                }
            };

            if start_offset > 0 {
                tokens.push(Token::Literal(snippet[..start_offset].to_string()));
            }

            position += start_offset + OPENER.len();

            let snippet = &template_text[position..];
            let end_offset = snippet.find(CLOSER)?;

            let placeholder_name = &snippet[..end_offset];
            tokens.push(Token::Placeholder(placeholder_name.to_string()));

            position += end_offset + CLOSER.len();
        }

        Some(Self { tokens })
    }

    pub fn extract_values(&self, text: &str) -> Option<HashMap<String, String>> {
        let mut values = HashMap::new();
        let mut remainder = text;

        let mut i = 0;
        while i < self.tokens.len() {
            match &self.tokens[i] {
                Token::Literal(literal) => {
                    remainder = remainder.strip_prefix(literal)?;
                }
                Token::Placeholder(name) => {
                    if let Some(Token::Literal(next_literal)) = self.tokens.get(i + 1) {
                        let pos = remainder.find(next_literal)?;
                        let extracted = &remainder[..pos];
                        values.insert(name.clone(), extracted.to_string());

                        remainder = &remainder[pos..];
                    } else {
                        values.insert(name.clone(), remainder.to_string());
                        remainder = "";
                    }
                }
            }
            i += 1;
        }

        if !remainder.is_empty() {
            return None;
        }

        Some(values)
    }
}

#[cfg(test)]
mod tests {
    use crate::token_template::TokenTemplate;

    struct TestData<'a> {
        template: &'a str,
        text: &'a str,
        expected_results: Vec<(&'a str, &'a str)>,
    }

    // cargo test test_tokens -- --nocapture
    #[test]
    fn test_token_template() {
        let mut test_data = vec![
            TestData {
                template: "{{Percentage}}% Chance to cast level {{Level}} Tornado on striking",
                text: "20% Chance to cast level 18 Tornado on striking",
                expected_results: vec![("Percentage", "20"), ("Level", "18")],
            },
            TestData {
                template: "+{{Life}} to Life",
                text: "+2 to Life",
                expected_results: vec![("Life", "2")],
            },
            TestData {
                template: "+{{Amount}} to {{Stat}}",
                text: "+20 to Dexterity",
                expected_results: vec![("Amount", "20"), ("Stat", "Dexterity")],
            },
            TestData {
                template: "Adds {{Min}}-{{Max}} {{DamageType}} damage",
                text: "Adds 5-30 fire damage",
                expected_results: vec![("Min", "5"), ("Max", "30"), ("DamageType", "fire")],
            },
            TestData {
                template: "Damage Reduced by {{Percentage}}%",
                text: "Damage Reduced by 10%",
                expected_results: vec![("Percentage", "10")],
            },
            TestData {
                template: "Damage Reduced by {{Percentage}}",
                text: "Damage Reduced by 15%",
                expected_results: vec![("Percentage", "15%")],
            },
            TestData {
                template: "Damage Reduced by {{Percentage}}%",
                text: "Damage Reduced by 25% extra text",
                expected_results: vec![],
            },
            TestData {
                template: "Random text",
                text: "Random text",
                expected_results: vec![],
            },
            TestData {
                template: "",
                text: "",
                expected_results: vec![],
            },
        ];

        for t_d in test_data.iter_mut() {
            _test_tokens(t_d);
        }
    }

    fn _test_tokens(test_data: &mut TestData) {
        let template = TokenTemplate::new(&test_data.template);

        if template.is_none() && test_data.expected_results.is_empty() {
            return;
        }

        let values = template.unwrap().extract_values(&test_data.text);

        if values.is_none() && test_data.expected_results.is_empty() {
            return;
        }

        let values = values.unwrap();

        let mut values: Vec<(&str, &str)> = values
            .iter()
            .map(|(k, v)| (k.as_str(), v.as_str()))
            .collect();

        values.sort_by(|a, b| a.0.cmp(&b.0));

        test_data.expected_results.sort_by(|a, b| a.0.cmp(&b.0));

        assert_eq!(test_data.expected_results, values);
    }
}
