use strum_macros::EnumIter;

use crate::{
    constants::validation_pixels::{
        AUTOMAP_FADE_NO_VALIDATION_PIXELS, AUTOMAP_OPTIONS_MENU_VALIDATION_PIXELS,
        AUTOMAP_SHOW_PARTY_NO_VALIDATION_PIXELS, AUTOMAP_SIZE_FULL_VALIDATION_PIXELS,
        BELT_OPEN_VALIDATION_PIXELS, DIFFICULTY_MENU_VALIDATION_PIXELS,
        EXIT_GAME_VALIDATION_PIXELS, HAS_DIED_VALIDATION_PIXELS, INVENTORY_OPEN_VALIDATION_PIXELS,
        IN_GAME_VALIDATION_PIXELS, LIGHTING_QUALITY_LOW_VALIDATION_PIXELS, MENU_VALIDATION_PIXELS,
        MERCENARY_WINDOW_VALIDATION_PIXELS, MERCHANT_TRADE_WINDOW_OPEN_VALIDATION_PIXELS,
        OPTIONS_MENU_VALIDATION_PIXELS, SINGLE_PLAYER_MENU_VALIDATION_PIXELS,
        STASH_VALIDATION_PIXELS, VIDEO_OPTIONS_MENU_VALIDATION_PIXELS,
        WAYPOINT_MENU_VALIDATION_PIXELS,
    },
    image::Image,
    state_validator::ValidationPixel,
};

#[derive(Clone, Copy, Debug, EnumIter)]
pub enum State {
    AutomapFadeNo,
    AutomapOptionsMenu,
    AutomapShowPartyNo,
    AutomapSizeFull,
    DifficultyMenu,
    MainMenu,
    HasDied,
    InGame,
    LightingQualityLow,
    Menu,
    OptionsMenu,
    SinglePlayerMenu,
    Stash,
    VideoOptionsMenu,
    WaypointMenu,
    InventoryOpen,
    BeltOpen,
    MerchantTradeWindowOpen,
    MercenaryWindowOpen,
}

impl State {
    pub fn is_active(&self, img: &Image) -> bool {
        let validation_pixels = self.get_validation_pixels();

        let mut valid_count = 0;

        for validation_pixel in validation_pixels {
            if img.get_value(validation_pixel.point) == validation_pixel.pixel {
                valid_count += 1;
            }
        }

        (valid_count as f32 / validation_pixels.len() as f32) > 0.8
    }

    fn get_validation_pixels(&self) -> &'static [ValidationPixel] {
        match self {
            Self::AutomapFadeNo => &AUTOMAP_FADE_NO_VALIDATION_PIXELS,
            Self::AutomapOptionsMenu => &AUTOMAP_OPTIONS_MENU_VALIDATION_PIXELS,
            Self::AutomapShowPartyNo => &AUTOMAP_SHOW_PARTY_NO_VALIDATION_PIXELS,
            Self::AutomapSizeFull => &AUTOMAP_SIZE_FULL_VALIDATION_PIXELS,
            Self::DifficultyMenu => &DIFFICULTY_MENU_VALIDATION_PIXELS,
            Self::MainMenu => &EXIT_GAME_VALIDATION_PIXELS,
            Self::HasDied => &HAS_DIED_VALIDATION_PIXELS,
            Self::InGame => &IN_GAME_VALIDATION_PIXELS,
            Self::LightingQualityLow => &LIGHTING_QUALITY_LOW_VALIDATION_PIXELS,
            Self::Menu => &MENU_VALIDATION_PIXELS,
            Self::OptionsMenu => &OPTIONS_MENU_VALIDATION_PIXELS,
            Self::SinglePlayerMenu => &SINGLE_PLAYER_MENU_VALIDATION_PIXELS,
            Self::Stash => &STASH_VALIDATION_PIXELS,
            Self::VideoOptionsMenu => &VIDEO_OPTIONS_MENU_VALIDATION_PIXELS,
            Self::WaypointMenu => &WAYPOINT_MENU_VALIDATION_PIXELS,
            Self::InventoryOpen => &INVENTORY_OPEN_VALIDATION_PIXELS,
            Self::BeltOpen => &BELT_OPEN_VALIDATION_PIXELS,
            Self::MerchantTradeWindowOpen => &MERCHANT_TRADE_WINDOW_OPEN_VALIDATION_PIXELS,
            Self::MercenaryWindowOpen => &MERCENARY_WINDOW_VALIDATION_PIXELS,
        }
    }
}
