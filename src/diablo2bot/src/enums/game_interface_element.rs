use serde::{Deserialize, Serialize};

use super::state::State;

#[derive(Clone, Copy, Hash, PartialEq, Eq, Serialize, Deserialize, Debug)]
pub enum GameInterfaceElement {
    Automap,
    Inventory,
    Belt,
    Items,
    Portraits,
    Chat,
    MercenaryWindow,
}

impl GameInterfaceElement {
    pub fn to_state(&self) -> Option<State> {
        match self {
            Self::Inventory => Some(State::InventoryOpen),
            Self::Belt => Some(State::BeltOpen),
            Self::MercenaryWindow => Some(State::MercenaryWindowOpen),
            Self::Automap => None,
            Self::Items => None,
            Self::Portraits => None,
            Self::Chat => None,
        }
    }
}
