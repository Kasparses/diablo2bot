use crate::{
    buy_potions::{find_merchant_dialog_option_point, MatchType},
    enums::{
        act::Act, click_type::ClickType, errors::WrongGameStateError,
        game_interface_element::GameInterfaceElement,
    },
    find_npc::find_mercenary_hire_npc,
    game::Game,
    game_interface_element_controller::GameInterfaceElementController,
    identify_items::exit_dialog_menu,
    point_u16::PointU16,
    route_walker::walk_enum_route_without_end_state,
    state_validator::wait_for_merchant_dialog_menu,
};

pub fn is_mercenary_alive(g: &mut Game) -> bool {
    match GameInterfaceElementController::activate_element(g, GameInterfaceElement::MercenaryWindow)
    {
        Ok(_) => {
            GameInterfaceElementController::deactivate_element(
                g,
                GameInterfaceElement::MercenaryWindow,
            )
            .unwrap();
            true
        }
        Err(_) => false,
    }
}

pub fn resurrect_mercenary(g: &mut Game) -> Result<(), WrongGameStateError> {
    walk_to_mercenary_hire_npc(g);

    match _find_mercenary_hire_npc(g) {
        Some(mercenary_hire_npc_point) => {
            click_on_mercenary_hire_npc(g, mercenary_hire_npc_point);

            resurrect(g)?;

            exit_dialog_menu(g);
            Ok(())
        }
        None => {
            // TODO Throw error
            panic!("Could not find the mercenary hire npc :(");
        }
    }
}

fn _find_mercenary_hire_npc(g: &mut Game) -> Option<PointU16> {
    match g.profile.zone_to_farm.to_act() {
        Act::Act1 | Act::Act2 | Act::Act3 | Act::Act5 => find_mercenary_hire_npc(g),
        Act::Act4 => Some(PointU16 { row: 150, col: 350 }),
    }
}

fn walk_to_mercenary_hire_npc(g: &mut Game) {
    walk_enum_route_without_end_state(
        g,
        g.profile
            .zone_to_farm
            .to_act()
            .get_mercenary_hire_npc_route(),
    );
}

fn click_on_mercenary_hire_npc(g: &mut Game, mercenary_hire_npc_point: PointU16) {
    g.output_controller
        .click_mouse(mercenary_hire_npc_point, ClickType::Left, true, true);

    g.output_controller.move_mouse_to_safe_point();
}

fn resurrect(g: &mut Game) -> Result<(), WrongGameStateError> {
    let merchant_dialog_matrix = wait_for_merchant_dialog_menu(g, 100)?;

    if let Some(resurrect_point) = find_merchant_dialog_option_point(
        g,
        &merchant_dialog_matrix,
        "Resurrect",
        MatchType::StartsWith,
    ) {
        g.output_controller
            .click_mouse(resurrect_point, ClickType::Left, true, true);
    }

    Ok(())
}
